%global d_bin                   %{_bindir}
%global d_home                  /home

%global d_storage_00            %{d_home}/storage_00
%global d_app                   %{d_storage_00}/app
%global d_usr                   %{d_storage_00}/usr
%global d_web                   %{d_storage_00}/web

%global d_storage_01            %{d_home}/storage_01
%global d_data_00               %{d_storage_01}/data_00
%global d_data_01               %{d_storage_01}/data_01
%global d_data_02               %{d_storage_01}/data_02
%global d_data_03               %{d_storage_01}/data_03
%global d_data_04               %{d_storage_01}/data_04
%global d_data_05               %{d_storage_01}/data_05
%global d_data_06               %{d_storage_01}/data_06
%global d_data_07               %{d_storage_01}/data_07
%global d_data_08               %{d_storage_01}/data_08
%global d_data_09               %{d_storage_01}/data_09

%global d_sysctl                %{_sysconfdir}/sysctl.d

Name:                           meta-system
Version:                        1.0.6
Release:                        1%{?dist}
Summary:                        META-package for configure system
License:                        GPLv3

Source10:                       sysctl.custom.conf

Requires:                       wget zsh

%description
META-package for configure system.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

# Directory: app.
%{__install} -dp -m 0755 %{buildroot}%{d_app}

# Directory: usr.
%{__install} -dp -m 0755 %{buildroot}%{d_usr}

# Directory: web.
%{__install} -dp -m 0755 %{buildroot}%{d_web}

# Directory: data_00.
%{__install} -dp -m 0755 %{buildroot}%{d_data_00}

# Directory: data_01.
%{__install} -dp -m 0755 %{buildroot}%{d_data_01}

# Directory: data_02.
%{__install} -dp -m 0755 %{buildroot}%{d_data_02}

# Directory: data_03.
%{__install} -dp -m 0755 %{buildroot}%{d_data_03}

# Directory: data_04.
%{__install} -dp -m 0755 %{buildroot}%{d_data_04}

# Directory: data_05.
%{__install} -dp -m 0755 %{buildroot}%{d_data_05}

# Directory: data_06.
%{__install} -dp -m 0755 %{buildroot}%{d_data_06}

# Directory: data_07.
%{__install} -dp -m 0755 %{buildroot}%{d_data_07}

# Directory: data_08.
%{__install} -dp -m 0755 %{buildroot}%{d_data_08}

# Directory: data_09.
%{__install} -dp -m 0755 %{buildroot}%{d_data_09}

# Install: configs.
%{__install} -Dp -m 0644 %{SOURCE10} \
  %{buildroot}%{d_sysctl}/00-sysctl.custom.conf

%files
# Directory: storage_00.
%dir %{d_storage_00}
%dir %{d_app}
%dir %{d_usr}
%dir %{d_web}

# Directory: storage_01.
%dir %{d_storage_01}
%dir %{d_data_00}
%dir %{d_data_01}
%dir %{d_data_02}
%dir %{d_data_03}
%dir %{d_data_04}
%dir %{d_data_05}
%dir %{d_data_06}
%dir %{d_data_07}
%dir %{d_data_08}
%dir %{d_data_09}

# Configs.
%config %{d_sysctl}/00-sysctl.custom.conf


%changelog
* Sat Oct 19 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.6-1
- UPD: SPEC-file.
- DEL: Scripts.

* Sun Aug 04 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.5-1
- NEW: USB install script.
- UPD: Shell scripts.

* Fri Aug 02 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.4-5
- UPD: Shell scripts.

* Thu Aug 01 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.4-4
- UPD: Shell scripts.

* Thu Aug 01 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.4-3
- UPD: Shell scripts.

* Wed Jul 31 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.4-2
- UPD: SPEC-file.

* Mon Jul 22 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.4-1
- NEW: "run.sum.sh".

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.3-11
- UPD: Shell scripts.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.3-10
- UPD: Shell scripts.

* Sat Jul 06 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.3-9
- UPD: Shell scripts.

* Sat Jul 06 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.3-8
- UPD: "run.domain.sh".

* Sat Jul 06 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.3-7
- UPD: "run.acme.sh".

* Fri Jul 05 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.3-6
- UPD: Shell scripts.

* Fri Jul 05 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.3-5
- UPD: "run.acme.sh".

* Fri Jul 05 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.3-4
- UPD: SPEC-file.

* Fri Jul 05 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.3-3
- UPD: Shell scripts.

* Fri Jul 05 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.3-2
- FIX: Shell scripts.
- FIX: sysctl.

* Thu Jul 04 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.3-1
- NEW: Shell scripts.

* Tue Jul 02 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.2-5
- UPD: SPEC-file.

* Tue Jul 02 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.2-4
- UPD: Directory structure.

* Sun Apr 14 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.2-3
- UPD: Directory structure.

* Sun Apr 14 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.2-2
- UPD: Directory structure.

* Sun Apr 14 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.2-1
- UPD: Directory structure.
- DEL: System scripts.

* Sun Apr 14 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.1-2
- UPD: Directory structure.

* Sat Apr 13 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.1-1
- UPD: Directory structure.
- NEW: User "sys_backup".

* Sat Apr 13 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-5
- UPD: Directory structure.

* Mon Apr 08 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-4
- UPD: Requires.

* Sat Mar 30 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-3
- UPD: Directory structure.

* Sat Mar 30 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-2
- UPD: Shell scripts.

* Wed Jan 02 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-1
- Initial build.
