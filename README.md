# Information / Информация

SPEC-файл для создания RPM-пакета **meta-system**.

## Структура хранилищ

```
/home
    /storage_00                 Storage: Internal.
        /app                    Users: Apps.
        /usr                    Users: System.
        /web                    Users: Web.
    /storage_01                 Storage: External.
        /data_00                Data: System & Private.
            /acme
            /...
        /data_01                Data: Cache / Logs / Temp / etc.
            /cache
            /log
            /session
            /tmp
        /data_02                Data: DB.
            /mysql
            /redis
            /...
        /data_03                Data: App.
            /app_0000
            /app_0001
            /...
        /data_04                Data: Web.
            /web_0000
            /web_0001
            /...
        /data_05
        /data_06
        /data_07
        /data_08
        /data_09
```

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/meta`.
2. Установить пакет: `dnf install meta-system`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)